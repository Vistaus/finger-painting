import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.0
import Ubuntu.Content 1.1

import Utils 1.0

PopupBase {
    id: importDialog
    anchors.fill: parent
    property var activeTransfer

    signal imported(string path)

    ContentPeerPicker {
        anchors.fill: parent
        visible: parent.visible
        contentType: ContentType.Pictures
        handler: ContentHandler.Source

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single
            importDialog.activeTransfer = peer.request()
            importDialog.activeTransfer.stateChanged.connect(function() {
                if (importDialog.activeTransfer && importDialog.activeTransfer.state === ContentTransfer.Charged) {
                    importDialog.imported(importDialog.activeTransfer.items[0].url);
                    importDialog.activeTransfer = null;
                    PopupUtils.close(importDialog);
                }
            })
        }

        onCancelPressed: {
            PopupUtils.close(importDialog);
        }
    }

    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: importDialog.activeTransfer
    }

    Component {
        id: resultComponent
        ContentItem {}
    }
}
